
# Nvim

## Getting started

## Installation
1. Install Packer:
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```
2. My Nvim-config:
```
git clone https://gitlab.com/sharad777/nvim.git ~/.config/nvim
nvim +PackerSync
```
