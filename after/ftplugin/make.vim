" setlocal noexpandtab
" " https://mattn.kaoriya.net/software/vim/20070821175457.htm
" setlocal isfname-== isfname+=32 isfname-=I isfname-=L
" setlocal shiftwidth=2
" setlocal softtabstop=2
" setlocal tabstop=2
im df <C-j><CR>
im fd <C-j><CR>
im dk <C-k><CR>
im kd <C-k><CR>
